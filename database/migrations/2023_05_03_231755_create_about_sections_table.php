<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAboutSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about_sections', function (Blueprint $table) {
            $table->id();
            $table->text("ar_first_section");
            $table->text("ar_second_section");
            $table->text("ar_third_section");

            $table->text("en_first_section");
            $table->text("en_second_section");
            $table->text("en_third_section");

            $table->text("tr_first_section");
            $table->text("tr_second_section");
            $table->text("tr_third_section");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about_sections');
    }
}
