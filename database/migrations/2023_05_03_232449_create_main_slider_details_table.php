<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMainSliderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main_slider_details', function (Blueprint $table) {
            $table->id();
            $table->integer('slider_id');

            $table->text('ar_header')->nullable();
            $table->text('en_header')->nullable();
            $table->text('tr_header')->nullable();

            $table->text('ar_paragraph')->nullable();
            $table->text('en_paragraph')->nullable();
            $table->text('tr_paragraph')->nullable();

            $table->text('image');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('main_slider_details');
    }
}
