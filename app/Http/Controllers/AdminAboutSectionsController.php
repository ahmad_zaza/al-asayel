<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\DB;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use crocodicstudio\crudbooster\controllers\CBController;

class AdminAboutSectionsController extends CBController
{

    public function cbInit()
    {

        # START CONFIGURATION DO NOT REMOVE THIS LINE
        $this->title_field = "id";
        $this->limit = "20";
        $this->orderby = "id,desc";
        $this->sortable_table = false;
        $this->global_privilege = false;
        $this->button_table_action = true;
        $this->button_bulk_action = true;
        $this->button_action_style = "button_icon";
        $this->record_seo = false;
        $this->button_add = false;
        $this->button_edit = true;
        $this->button_delete = false;
        $this->button_detail = true;
        $this->pdf_direction = "ltr";
        $this->button_show = true;
        $this->button_filter = true;
        $this->button_import = false;
        $this->button_export = false;
        $this->page_seo = false;
        $this->table = "about_sections";
        # END CONFIGURATION DO NOT REMOVE THIS LINE

        # START COLUMNS DO NOT REMOVE THIS LINE
        $this->col = [];
        $this->col[] = ["label" => "Ar First Section", "name" => "ar_first_section", "str_limit" => 150];
        $this->col[] = ["label" => "Ar Second Section", "name" => "ar_second_section", "str_limit" => 150];
        $this->col[] = ["label" => "Ar Third Section", "name" => "ar_third_section", "str_limit" => 150];
        $this->col[] = ["label" => "En First Section", "name" => "en_first_section", "str_limit" => 150];
        $this->col[] = ["label" => "En Second Section", "name" => "en_second_section", "str_limit" => 150];
        $this->col[] = ["label" => "En Third Section", "name" => "en_third_section", "str_limit" => 150];
        $this->col[] = ["label" => "Tr First Section", "name" => "tr_first_section", "str_limit" => 150];
        # END COLUMNS DO NOT REMOVE THIS LINE

        # START FORM DO NOT REMOVE THIS LINE
        $this->form = [];
        $this->form[] = ['label' => 'Ar First Section', 'name' => 'ar_first_section', 'type' => 'textarea', 'validation' => 'string|min:5|max:5000', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Ar Second Section', 'name' => 'ar_second_section', 'type' => 'textarea', 'validation' => 'string|min:5|max:5000', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Ar Third Section', 'name' => 'ar_third_section', 'type' => 'textarea', 'validation' => 'string|min:5|max:5000', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'En First Section', 'name' => 'en_first_section', 'type' => 'textarea', 'validation' => 'string|min:5|max:5000', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'En Second Section', 'name' => 'en_second_section', 'type' => 'textarea', 'validation' => 'string|min:5|max:5000', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'En Third Section', 'name' => 'en_third_section', 'type' => 'textarea', 'validation' => 'string|min:5|max:5000', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Tr First Section', 'name' => 'tr_first_section', 'type' => 'textarea', 'validation' => 'string|min:5|max:5000', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Tr Second Section', 'name' => 'tr_second_section', 'type' => 'textarea', 'validation' => 'string|min:5|max:5000', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Tr Third Section', 'name' => 'tr_third_section', 'type' => 'textarea', 'validation' => 'string|min:5|max:5000', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Second Section Image', 'name' => 'second_section_img', 'type' => 'text'];
        $this->form[] = ['label' => 'Third Section Image', 'name' => 'third_section_img', 'type' => 'text'];
        $this->form[] = ['label' => 'Page Image', 'name' => 'page_image', 'type' => 'filemanager', 'validation' => 'required'];

        $this->sub_module = array();

        $this->addaction = array();

        $this->button_selected = array();

        $this->alert        = array();

        $this->index_button = array();

        $this->table_row_color = array();

        $this->index_statistic = array();

        $this->script_js = NULL;

        $this->pre_index_html = null;

        $this->post_index_html = null;

        $this->load_js = array();

        $this->style_css = NULL;

        $this->load_css = array();
    }


    public function actionButtonSelected($id_selected, $button_name)
    {
        //Your code here

    }

    public function hook_query_index(&$query)
    {
        //Your code here

    }

    public function hook_row_index($column_index, &$column_value)
    {
        //Your code here
    }

    public function hook_before_add(&$postdata)
    {
        //Your code here

    }

    public function hook_after_add($id)
    {
        //Your code here

    }

    public function hook_before_edit(&$postdata, $id)
    {
        //Your code here

    }

    public function hook_after_edit($id)
    {
        //Your code here

    }

    public function hook_before_delete($id)
    {
        //Your code here

    }

    public function hook_after_delete($id)
    {
        //Your code here

    }
}
