<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::all();
        $settings = DB::table('cms_seo')->where([
            'page' => 'products',
            'language' => App::getlocale(),
        ])->get()[0];

        return view('website.products', compact('products', 'settings'));
    }

    public function showProduct($id)
    {
        $product = Product::where('id', $id)->with(['productDetails' => function ($query) {
            $query->orderBy('sorting', 'asc');
        }])->with('productColors')
            ->first();
        $product_images = DB::table('model_images')
            ->where([
                'model_type' => 'products',
                'model_id' => $id
            ])->get();
        $settings = DB::table('cms_seo')->where([
            'page' => 'products',
            'page_id' => $id,
            'language' => App::getlocale(),
        ])->get()[0];
        if (!$settings)
            $settings = DB::table('cms_seo')->where('page', 'home')->where('language', App::getlocale())->first();
        return view('website.product', compact('product', 'product_images', 'settings'));
    }
}
