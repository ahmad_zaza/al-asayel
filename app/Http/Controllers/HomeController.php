<?php

namespace App\Http\Controllers;

use App\Models\ContactSection;
use App\Models\AboutSection;
use App\Models\Client;
use App\Models\ContactRequest;
use App\Models\MainSlider;
use App\Models\Product;
use App\Models\SocialMedia;
use App\Rules\Recaptcha;
use App\Models\Workflow;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    public function index()
    {
        $mainSliders = MainSlider::with('mainSlidersDetails')->get()[0]->mainSlidersDetails;
        $aboutSection = AboutSection::first();
        $contactSection = ContactSection::first();
        $products = Product::all();
        $workkflows = Workflow::all();
        $clients = Client::all();
        $socialMedia = SocialMedia::all();
        $settings = DB::table('cms_seo')->where('page', 'home')->where('language', App::getLocale())->first();
        return view('website.home', compact('mainSliders', 'aboutSection', 'contactSection', 'products', 'settings', 'workkflows', 'socialMedia', 'clients'));
    }

    public function showAboutUs()
    {
        $aboutSection = AboutSection::first();
        $settings = DB::table('cms_seo')->where('page', 'about_sections')->where('language', App::getlocale())->first();
        return view('website.about_us', compact('aboutSection', 'settings'));
    }

    public function showGallery()
    {
        $settings = DB::table('cms_seo')->where('page', 'gallery')->where('language', App::getlocale())->first();
        $gallery_images = DB::table('model_images')
            ->where([
                'model_type' => 'galleries',
            ])->get();
        return view('website.gallery', compact('settings', 'gallery_images'));
    }

    public function switchLanguage($code)
    {
        App::setlocale($code);
        Session::put("locale", $code);
        return redirect()->back();
    }

    public function sendContact(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|email',
            'phone_number' => 'required|numeric',
            'message' => 'required',
            'g-recaptcha-response' => ['required', new Recaptcha]
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }
        try {

            $contactRequest = ContactRequest::create([
                'name' => $request->name,
                'email' => $request->email,
                'ip_address' => $request->ip(),
                'phone_number' => $request->phone_number,
                'message' => $request->message
            ]);

            // CRUDBooster::sendEmail([
            //     'to' => ['export_alasayel@outlook.com', 'kubra_alasayel@outlook.com'],
            //     'data' => [
            //         'name' => $request->name,
            //         'phone_number' => $request->phone_number,
            //         'message' => $request->message
            //     ],
            //     'template' => 'contact_request'
            // ]);
            return back()->with(['message' => trans('data.successfully_send_request_us'), 'message_type' => 'success']);
        } catch (Exception $ex) {
            return back()->with(['message' => trans('data.successfully_send_request_us'), 'message_type' => 'danger']);
        }
    }
}
