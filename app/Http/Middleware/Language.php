<?php

namespace App\Http\Middleware;

use Closure;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Application;


class Language
{
    public function handle($request, Closure $next)
    {
        App::setLocale(Session::get('locale', config('app.locale')));
        if (!Session::has('locale')) {
            Session::put(App::getlocale());
        }
        return $next($request);
    }
}
