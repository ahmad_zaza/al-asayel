<?php

namespace App\Providers;

use App\Models\SiteMenu;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // $lang = App::getlocale();
        $menu = SiteMenu::where('active', 1)->get();
        $languages = DB::table('languages')->get();
        View::share('menu', $menu);
        // View::share('lang', $lang);
        View::share('languages', $languages);
        Schema::defaultStringLength(191);

    }
}
