<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactRequest extends Model
{
    public $table = 'contact_requests';

    protected $fillable = [
        'id',
        'name',
        'ip_address',
        'phone_number',
        'message'
    ];
}
