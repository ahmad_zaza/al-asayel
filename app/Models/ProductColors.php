<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductColors extends Model
{
    public $table = 'product_colors';

    protected $fillable = [
        'id',
        'code',
        'product_id'
    ];

    public function product(){
        return $this->belongsTo(Product::class, 'product_id');
    }
}
