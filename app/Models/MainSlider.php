<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MainSlider extends Model
{
    public $table = 'main_sliders';

    protected $fillable = [
        'id'
    ];

    public function mainSlidersDetails(){
        return $this->hasMany(MainSliderDetails::class, 'slider_id', 'id');
    }
}
