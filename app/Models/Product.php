<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $table = 'products';

    protected $fillable = [
        'id',
        'ar_name',
        'en_name',
        'tr_name',
        'main_img',
        'ar_description',
        'en_description',
        'tr_description',
        'sorting'
    ];

    public function productDetails()
    {
        return $this->hasMany(ProductDetails::class, 'product_id');
    }

    public function productColors()
    {
        return $this->hasMany(ProductColors::class, 'product_id');
    }
}
