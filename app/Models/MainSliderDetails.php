<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MainSliderDetails extends Model
{
    public $table = 'main_slider_details';

    protected $fillable = [
        'slider_id',
        'ar_header',
        'en_header',
        'tr_header',
        'ar_paragraph',
        'en_paragraph',
        'tr_paragraph',
        'image'
    ];

    public function MainSliders(){
        return $this->belongsTo(MainSlider::class, 'slider_id', 'id');
    }
}
