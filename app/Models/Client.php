<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    public $table = 'clients';

    protected $fillable = [
        'id',
        'arname',
        'en_name',
        'tr_name',
        'image'
    ];
}
