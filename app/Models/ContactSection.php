<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactSection extends Model
{
    public $table = 'contact_sections';

    protected $fillable = [
        'id',
        'ar_content',
        'en_content',
        'tr_content',
        'ar_address',
        'en_address',
        'tr_address',
        'f_phone_number',
        's_phone_number',
        'email'
    ];
}
