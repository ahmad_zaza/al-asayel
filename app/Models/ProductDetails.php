<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductDetails extends Model
{
    public $table = 'product_details';

    protected $fillable = [
        'id',
        'product_id',
        'ar_name',
        'en_name',
        'tr_name',
        'outer_diameter',
        'depth',
        'volume',
        'sorting'
    ];

    public function product(){
        return $this->belongsTo(Product::class, 'product_id');
    }
}
