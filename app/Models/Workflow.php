<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Workflow extends Model
{
    public $table = 'workflows';

    protected $fillable = [
        'ar_title',
        'en_title',
        'tr_title',
        'ar_description',
        'en_description',
        'tr_description',
        'icon'
    ];
}
