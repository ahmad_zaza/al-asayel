<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AboutSection extends Model
{
    public $table = 'about_sections';

    protected $fillable = [
        'ar_first_section',
        'en_second_section',
        'tr_third_section',

        'ar_first_section',
        'en_second_section',
        'tr_third_section',

        'ar_first_section',
        'en_second_section',
        'tr_third_section'
    ];
}
