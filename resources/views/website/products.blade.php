@extends('website.layouts.app')

@section('title')
    {{ $settings->title }}
@endsection

@section('content')
    @php
        $lang = App::getlocale();
    @endphp
    <section class="product-title">
        <div class="container">
            <h1 class="main-title-underline">{{ __('products') }}</h1>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="cards-section">
                <div class="row card-deck">
                    @foreach ($products as $key => $product)
                        <div class="col-md-4 col-sm-6 col-xs-12" style="padding:5px;">
                            <div class="prod-cards prod-cards-{{ $key + 1 }}" id="{{ $key + 1 }}"
                                style="position: relative;">
                                <a class="prod-card-overlay prod-card-overlay-{{ $key + 1 }}"
                                    href="{{ route('product.index', [$product->id]) }}"></a>
                                <a class="product-link" href="{{ route('product.index', [$product->id]) }}">
                                    <img class="card-img-top" src="{{ asset($product['main_img']) }}" alt="Card image cap"
                                        loading="lazy">
                                    <div class="card-footer card-footer-{{ $key + 1 }}">
                                        <small
                                            class="card-text-muted card-text-muted-{{ $key + 1 }}">{{ $product[$lang . '_name'] }}</small>
                                    </div>
                                    <div class="transparent-card-footer transparent-card-footer-{{ $key + 1 }}">
                                        <p
                                            class="transparent-card-text-muted transparent-card-text-muted-{{ $key + 1 }}">
                                            {{ __('view') }}</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <script>
                            let myElement_{{ $key + 1 }} = document.querySelector('.prod-cards-{{ $key + 1 }}');
                            myElement_{{ $key + 1 }}.addEventListener('mouseover', function() {
                                let id = @json($key + 1);
                                console.log("over", id);
                                $('.prod-card-overlay-' + id).css('display', 'block');
                                $('.card-footer-' + id).css('display', 'none');
                                $('.transparent-card-footer-' + id).css('display', 'block');
                            });

                            myElement_{{ $key + 1 }}.addEventListener('mouseout', function() {
                                let id = @json($key + 1);
                                console.log("out", id);
                                $('.prod-card-overlay-' + id).css('display', 'none');
                                $('.card-footer-' + id).css('display', 'block');
                                $('.transparent-card-footer-' + id).css('display', 'none');
                            });
                        </script>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection
