@extends('website.layouts.app')

@section('title')
    {{ $settings->title }}
@endsection

@section('content')
    @php
        $lang = App::getlocale();
    @endphp
    <section class="about-page-section">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12 bordered-col">
                    <div class="about-text">
                        <h1 class="main-title" style="margin-bottom:15px;">
                            {{ __('about') }}
                        </h1>
                        <p class="padded-paragraph-15">
                            {{ $aboutSection[$lang . '_first_section'] }}
                        </p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="about-page-img">
                        <img src="{{ $aboutSection['page_image'] }}" loading="lazy" alt="">
                    </div>
                </div>
            </div>
            <div class="row about-row-down">
                <div class="col-md-6 col-sm-12">
                    <div class="svg-about">
                        <svg width="50px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g id="SVGRepo_bgCarrier" stroke-width="0"></g>
                            <g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g>
                            <g id="SVGRepo_iconCarrier">
                                <path
                                    d="M13 15C13 15.5523 12.5523 16 12 16C11.4477 16 11 15.5523 11 15C11 14.4477 11.4477 14 12 14C12.5523 14 13 14.4477 13 15Z"
                                    stroke="#bcbcbc" stroke-width="2"></path>
                                <path
                                    d="M15 9C16.8856 9 17.8284 9 18.4142 9.58579C19 10.1716 19 11.1144 19 13L19 15L19 17C19 18.8856 19 19.8284 18.4142 20.4142C17.8284 21 16.8856 21 15 21L12 21L9 21C7.11438 21 6.17157 21 5.58579 20.4142C5 19.8284 5 18.8856 5 17L5 15L5 13C5 11.1144 5 10.1716 5.58579 9.58579C6.17157 9 7.11438 9 9 9L12 9L15 9Z"
                                    stroke="#bcbcbc" stroke-width="2" stroke-linejoin="round"></path>
                                <path d="M9 9V5C9 3.89543 9.89543 3 11 3H13C14.1046 3 15 3.89543 15 5V9" stroke="#bcbcbc"
                                    stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                            </g>
                        </svg>
                    </div>
                    <div class="about-sub-title">
                        <h6>
                            {{ __('innovation') }}
                        </h6>
                    </div>
                    <p>
                        {{ $aboutSection[$lang . '_second_section'] }}
                    </p>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="svg-about">
                        <svg width="50px" viewBox="0 0 120 120" id="Layer_1" version="1.1" xml:space="preserve"
                            xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" fill="#000000">
                            <g id="SVGRepo_bgCarrier" stroke-width="0"></g>
                            <g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g>
                            <g id="SVGRepo_iconCarrier">
                                <style type="text/css">
                                    .st0 {
                                        fill: #bcbcbc;
                                    }

                                    .st1 {
                                        fill: #ffffff;
                                    }

                                    .st2 {
                                        fill: #ffffff;
                                    }
                                </style>
                                <g>
                                    <polygon class="st0"
                                        points="60,13.7 70.7,19.9 83.1,19.9 89.3,30.7 100.1,36.9 100.1,49.3 106.3,60 100.1,70.7 100.1,83.1 89.3,89.3 83.1,100.1 70.7,100.1 60,106.3 49.3,100.1 36.9,100.1 30.7,89.3 19.9,83.1 19.9,70.7 13.7,60 19.9,49.3 19.9,36.9 30.7,30.7 36.9,19.9 49.3,19.9 ">
                                    </polygon>
                                    <g>
                                        <path class="st1"
                                            d="M60,93.9c-18.7,0-33.9-15.2-33.9-33.9S41.3,26.1,60,26.1S93.9,41.3,93.9,60S78.7,93.9,60,93.9z M60,29 c-17.1,0-31,13.9-31,31s13.9,31,31,31s31-13.9,31-31S77.1,29,60,29z">
                                        </path>
                                    </g>
                                    <g>
                                        <path class="st2"
                                            d="M56.3,72.6L41.6,60.9c-1.2-1-1.4-2.7-0.4-3.9l0,0c1-1.2,2.7-1.4,3.9-0.4l12.6,10.1l16.8-18.8 c1-1.1,2.8-1.2,3.9-0.2v0c1.1,1,1.2,2.8,0.2,3.9L60.1,72.3C59.1,73.4,57.4,73.5,56.3,72.6z">
                                        </path>
                                    </g>
                                </g>
                            </g>
                        </svg>
                    </div>
                    <div class="about-sub-title">
                        <h6>
                            {{ __('quality') }}
                        </h6>
                    </div>
                    <p>
                        {{ $aboutSection[$lang . '_third_section'] }}
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection
