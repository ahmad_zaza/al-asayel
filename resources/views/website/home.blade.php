@extends('website.layouts.app')

@section('title')
    {{ $settings->title }}
@endsection

@section('content')
    @php
        $lang = App::getlocale();
    @endphp
    <main>
        <section class="main-slider">
            <div id="carouselMainIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    @foreach ($mainSliders as $key => $slider)
                        <li data-target="#carouselMainIndicators" data-slide-to="{{ $key }}" class="active">
                            <i class="fa fa-circle"></i>
                        </li>
                    @endforeach
                </ol>
                <div class="carousel-inner">
                    @foreach ($mainSliders as $key => $slider)
                        <div class="carousel-item {{ $key == 0 ? 'active' : '' }}">
                            <img class="d-block main-slide-img w-100" src="{{ asset($slider->image) }}" alt="First slide">
                        </div>
                    @endforeach
                </div>
                <a class="carousel-control-prev" href="#carouselMainIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselMainIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </section>
        <section class="about-section inner-section">
            <div class="container">
                <h1 class="main-title">
                    {{ __('about') }}
                </h1>
                <div class="row inner-row">
                    <div class="col-md-6 col-sm-12">
                        <p class="about-text">
                            {{ $aboutSection[$lang . '_first_section'] }}
                        </p>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="about-box f-about-box">
                            <div class="svg-about">
                                <svg width="50px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <g id="SVGRepo_bgCarrier" stroke-width="0"></g>
                                    <g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g>
                                    <g id="SVGRepo_iconCarrier">
                                        <path
                                            d="M13 15C13 15.5523 12.5523 16 12 16C11.4477 16 11 15.5523 11 15C11 14.4477 11.4477 14 12 14C12.5523 14 13 14.4477 13 15Z"
                                            stroke="#bcbcbc" stroke-width="2"></path>
                                        <path
                                            d="M15 9C16.8856 9 17.8284 9 18.4142 9.58579C19 10.1716 19 11.1144 19 13L19 15L19 17C19 18.8856 19 19.8284 18.4142 20.4142C17.8284 21 16.8856 21 15 21L12 21L9 21C7.11438 21 6.17157 21 5.58579 20.4142C5 19.8284 5 18.8856 5 17L5 15L5 13C5 11.1144 5 10.1716 5.58579 9.58579C6.17157 9 7.11438 9 9 9L12 9L15 9Z"
                                            stroke="#bcbcbc" stroke-width="2" stroke-linejoin="round"></path>
                                        <path d="M9 9V5C9 3.89543 9.89543 3 11 3H13C14.1046 3 15 3.89543 15 5V9"
                                            stroke="#bcbcbc" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round"></path>
                                    </g>
                                </svg>
                            </div>
                            <h6>{{ __('innovation') }}</h6>
                            <p class="about-text sub-about-text">
                                {{ $aboutSection[$lang . '_second_section'] }}
                            </p>
                        </div>
                        <div class="about-box s-about-box">
                            <div class="svg-about-2">
                                <svg width="50px" viewBox="0 0 120 120" id="Layer_1" version="1.1" xml:space="preserve"
                                    xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                    fill="#000000">
                                    <g id="SVGRepo_bgCarrier" stroke-width="0"></g>
                                    <g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g>
                                    <g id="SVGRepo_iconCarrier">
                                        <style type="text/css">
                                            .st0 {
                                                fill: #bcbcbc;
                                            }

                                            .st1 {
                                                fill: #ffffff;
                                            }

                                            .st2 {
                                                fill: #ffffff;
                                            }
                                        </style>
                                        <g>
                                            <polygon class="st0"
                                                points="60,13.7 70.7,19.9 83.1,19.9 89.3,30.7 100.1,36.9 100.1,49.3 106.3,60 100.1,70.7 100.1,83.1 89.3,89.3 83.1,100.1 70.7,100.1 60,106.3 49.3,100.1 36.9,100.1 30.7,89.3 19.9,83.1 19.9,70.7 13.7,60 19.9,49.3 19.9,36.9 30.7,30.7 36.9,19.9 49.3,19.9 ">
                                            </polygon>
                                            <g>
                                                <path class="st1"
                                                    d="M60,93.9c-18.7,0-33.9-15.2-33.9-33.9S41.3,26.1,60,26.1S93.9,41.3,93.9,60S78.7,93.9,60,93.9z M60,29 c-17.1,0-31,13.9-31,31s13.9,31,31,31s31-13.9,31-31S77.1,29,60,29z">
                                                </path>
                                            </g>
                                            <g>
                                                <path class="st2"
                                                    d="M56.3,72.6L41.6,60.9c-1.2-1-1.4-2.7-0.4-3.9l0,0c1-1.2,2.7-1.4,3.9-0.4l12.6,10.1l16.8-18.8 c1-1.1,2.8-1.2,3.9-0.2v0c1.1,1,1.2,2.8,0.2,3.9L60.1,72.3C59.1,73.4,57.4,73.5,56.3,72.6z">
                                                </path>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </div>
                            <h6>{{ __('quality') }}</h6>
                            <p class="about-text sub-about-text">
                                {{ $aboutSection[$lang . '_third_section'] }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="services-section inner-section">
            <div class="container">
                <h1 class="main-title">
                    {{ __('services') }}
                </h1>
                <div class="cards-section">
                    <div class="row card-deck">
                        @foreach ($products as $key => $product)
                            <div class="col-md-4 col-sm-6 col-xs-12" style="padding:5px;">
                                <div class="prod-cards prod-cards-{{ $key + 1 }}" id="{{ $key + 1 }}"
                                    style="position: relative;">
                                    <a class="prod-card-overlay prod-card-overlay-{{ $key + 1 }}"
                                        href="{{ route('product.index', [$product->id]) }}"></a>
                                    <a class="product-link" href="{{ route('product.index', [$product->id]) }}">
                                        <img class="card-img-top" src="{{ asset($product['main_img']) }}"
                                            alt="Card image cap" loading="lazy">
                                        <div class="card-footer card-footer-{{ $key + 1 }}">
                                            <small
                                                class="card-text-muted card-text-muted-{{ $key + 1 }}">{{ $product[$lang . '_name'] }}</small>
                                        </div>
                                        <div class="transparent-card-footer transparent-card-footer-{{ $key + 1 }}">
                                            <p
                                                class="transparent-card-text-muted transparent-card-text-muted-{{ $key + 1 }}">
                                                {{ __('view') }}</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <script>
                                let myElement_{{ $key + 1 }} = document.querySelector('.prod-cards-{{ $key + 1 }}');
                                myElement_{{ $key + 1 }}.addEventListener('mouseover', function() {
                                    let id = @json($key + 1);
                                    $('.prod-card-overlay-' + id).css('display', 'block');
                                    $('.card-footer-' + id).css('display', 'none');
                                    $('.transparent-card-footer-' + id).css('display', 'block');
                                });

                                myElement_{{ $key + 1 }}.addEventListener('mouseout', function() {
                                    let id = @json($key + 1);
                                    $('.prod-card-overlay-' + id).css('display', 'none');
                                    $('.card-footer-' + id).css('display', 'block');
                                    $('.transparent-card-footer-' + id).css('display', 'none');
                                });
                            </script>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
        <section class="top-workflow-section inner-section" style="position: relative;">
            <img class="hand-shake-img" src="{{ asset(CRUDBooster::getSetting('workflow_background')) }}"
                alt="">
        </section>
        <section class="workflow-section">
            <div class="workflow-cards">
                <div class="container">
                    <div class="row">
                        @foreach ($workkflows as $key => $workflow)
                            <div class="col-lg-3 col-md-6 col-sm-12 workflow-col">
                                <div class="workflow-box {{ 'workflow-box_' . $key }}">
                                    <div class="svg-icon">
                                        {!! $workflow['icon'] !!}
                                    </div>
                                    {{-- <div class="box-paragraph"> --}}
                                    <p class="workflow-title">{{ $workflow[$lang . '_title'] }}</h3>
                                    <p class="workflow-description">{{ $workflow[$lang . '_description'] }}</p>
                                    {{-- </div> --}}
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
        @if (!is_null($clients) && count($clients))
            <section class="clients-section inner-section">
                <div class="container">
                    <div class="row">
                        <h1 class="main-title">
                            {{ __('references') }}
                        </h1>
                    </div>
                    <div class="owl-carousel clients">
                        @foreach ($clients as $client)
                            <div class="item"><img class="owl-img" src="{{ asset($client['image']) }}"
                                    alt=""></div>
                        @endforeach
                    </div>
                </div>
            </section>
        @endif
        <section class="contact-section inner-section">
            <div class="container">
                <h1 class="main-title">
                    {{ __('contact_us') }}
                </h1>
                <div class="mab-box">
                    <iframe title="map"
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d669.9328704504035!2d28.99921941135091!3d41.07124559216155!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14cab657e2b19579%3A0x6b04cf8af2373f8f!2sGulbahar%20Neighborhood%20Muhtarligi!5e0!3m2!1sen!2sus!4v1683233246046!5m2!1sen!2sus"
                        width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"
                        referrerpolicy="no-referrer-when-downgrade"></iframe>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <p class="contact-text">
                            {{ $contactSection[$lang . '_content'] }} <br>
                        </p>
                        <p>
                            <span class="contact-span">
                                <i class="fa fa-map-marker contact-icon" aria-hidden="true"></i> {{ __('address') }}:
                            </span>
                            {{ $contactSection[$lang . '_address'] }}
                        </p>
                        <p>
                            <span class="contact-span">
                                <i class="fa fa-whatsapp contact-icon" aria-hidden="true"></i> {{ __('phone_number') }}:
                            </span>
                            {{ $contactSection['f_phone_number'] }} / {{ $contactSection['s_phone_number'] }}
                        </p>
                        <p>
                            <span class="contact-span">
                                <i class="fa fa-envelope contact-icon" aria-hidden="true"></i> {{ __('email') }}:
                            </span>
                            {{ $contactSection['email'] }}
                        </p>
                        <div class="follow-us">
                            <h5 class="follow-us-title">
                                {{ __('follow_us') }}
                            </h5>
                            <ul class="list-group social-list-group">
                                @foreach ($socialMedia as $social_media)
                                    @if ($social_media['name'] == 'Facebook')
                                        <li class="list-item list-social-item-1">
                                            <a class="social-media-svg" href="{{ $social_media['url'] }}"
                                                aria-label="Facebook" target="blank">
                                                <svg fill="#472219" width="25px" version="1.1" id="Layer_1"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                    xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="-143 145 512 512"
                                                    xml:space="preserve">
                                                    <g id="SVGRepo_bgCarrier" stroke-width="0"></g>
                                                    <g id="SVGRepo_tracerCarrier" stroke-linecap="round"
                                                        stroke-linejoin="round">
                                                    </g>
                                                    <g id="SVGRepo_iconCarrier">
                                                        <path
                                                            d="M329,145h-432c-22.1,0-40,17.9-40,40v432c0,22.1,17.9,40,40,40h432c22.1,0,40-17.9,40-40V185C369,162.9,351.1,145,329,145z M169.5,357.6l-2.9,38.3h-39.3v133H77.7v-133H51.2v-38.3h26.5v-25.7c0-11.3,0.3-28.8,8.5-39.7c8.7-11.5,20.6-19.3,41.1-19.3 c33.4,0,47.4,4.8,47.4,4.8l-6.6,39.2c0,0-11-3.2-21.3-3.2c-10.3,0-19.5,3.7-19.5,14v29.9H169.5z">
                                                        </path>
                                                    </g>
                                                </svg>
                                            </a>
                                        </li>
                                    @elseif($social_media['name'] == 'Youtube')
                                        <li class="list-item list-social-item-3">
                                            <a class="social-media-svg" href="{{ $social_media['url'] }}"
                                                aria-label="Youtube" target="blank">
                                                <svg fill="#472219" width="25px" version="1.1" id="Layer_1"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                    xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="-143 145 512 512"
                                                    xml:space="preserve">
                                                    <g id="SVGRepo_bgCarrier" stroke-width="0"></g>
                                                    <g id="SVGRepo_tracerCarrier" stroke-linecap="round"
                                                        stroke-linejoin="round">
                                                    </g>
                                                    <g id="SVGRepo_iconCarrier">
                                                        <g>
                                                            <polygon points="78.9,450.3 162.7,401.1 78.9,351.9 "></polygon>
                                                            <path
                                                                d="M329,145h-432c-22.1,0-40,17.9-40,40v432c0,22.1,17.9,40,40,40h432c22.1,0,40-17.9,40-40V185C369,162.9,351.1,145,329,145z M241,446.8L241,446.8c0,44.1-44.1,44.1-44.1,44.1H29.1c-44.1,0-44.1-44.1-44.1-44.1v-91.5c0-44.1,44.1-44.1,44.1-44.1h167.8 c44.1,0,44.1,44.1,44.1,44.1V446.8z">
                                                            </path>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </a>
                                        </li>
                                    @else
                                        <li class="list-item list-social-item-2">
                                            <a class="social-media-svg" href="{{ $social_media['url'] }}"
                                                aria-label="Instagram" target="blank">
                                                <svg width="30px" viewBox="0 0 32 32" id="Camada_1" version="1.1"
                                                    xml:space="preserve" xmlns="http://www.w3.org/2000/svg"
                                                    xmlns:xlink="http://www.w3.org/1999/xlink" fill="#472219"
                                                    style="margin-top: -2px;">
                                                    <g id="SVGRepo_bgCarrier" stroke-width="0"></g>
                                                    <g id="SVGRepo_tracerCarrier" stroke-linecap="round"
                                                        stroke-linejoin="round">
                                                    </g>
                                                    <g id="SVGRepo_iconCarrier">
                                                        <style type="text/css">
                                                            .st0 {
                                                                fill: #FFFFFF;
                                                            }
                                                        </style>
                                                        <path
                                                            d="M6,2h20c2.2,0,4,1.8,4,4v20c0,2.2-1.8,4-4,4H6c-2.2,0-4-1.8-4-4V6C2,3.8,3.8,2,6,2z">
                                                        </path>
                                                        <g>
                                                            <path class="st0"
                                                                d="M21.3,9.7c-0.6,0-1.2,0.5-1.2,1.2c0,0.7,0.5,1.2,1.2,1.2c0.7,0,1.2-0.5,1.2-1.2C22.4,10.2,21.9,9.7,21.3,9.7z">
                                                            </path>
                                                            <path class="st0"
                                                                d="M16,11.2c-2.7,0-4.9,2.2-4.9,4.9c0,2.7,2.2,4.9,4.9,4.9s4.9-2.2,4.9-4.9C21,13.4,18.8,11.2,16,11.2z M16,19.3 c-1.7,0-3.2-1.4-3.2-3.2c0-1.7,1.4-3.2,3.2-3.2c1.7,0,3.2,1.4,3.2,3.2C19.2,17.9,17.8,19.3,16,19.3z">
                                                            </path>
                                                            <path class="st0"
                                                                d="M20,6h-8c-3.3,0-6,2.7-6,6v8c0,3.3,2.7,6,6,6h8c3.3,0,6-2.7,6-6v-8C26,8.7,23.3,6,20,6z M24.1,20 c0,2.3-1.9,4.1-4.1,4.1h-8c-2.3,0-4.1-1.9-4.1-4.1v-8c0-2.3,1.9-4.1,4.1-4.1h8c2.3,0,4.1,1.9,4.1,4.1V20z">
                                                            </path>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="contact-form-box">
                            <form class="contact_us-form" action="{{ route('contact.send') }}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <input name="name" type="text" class="form-control" id="name-input"
                                        placeholder="{{ __('name') }}" required>
                                </div>
                                <div class="form-group">
                                    <input name="email" type="email" class="form-control" id="email-input"
                                        placeholder="{{ __('email') }}" required>
                                </div>
                                <div class="form-group">
                                    <input name="phone_number" type="text" class="form-control" id="phone-input"
                                        placeholder="{{ __('phone') }}" required>
                                </div>
                                <div class="form-group">
                                    <textarea name="message" class="form-control" id="input-message" rows="3" placeholder="{{ __('message') }}"
                                        required></textarea>
                                </div>
                                <div class="form-group">
                                    <div class="g-recaptcha"
                                        data-sitekey="{{ CRUDBooster::getSetting('website_site_key') }}"></div>
                                    @if ($errors->has('g-recaptcha-response'))
                                        <span class="text-danger">{{ $errors->first('g-recaptcha-response') }}</span>
                                    @endif
                                </div>
                                <button class="contact-submit-button" type="submit"
                                    class="btn btn-primary">{{ __('send') }}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
