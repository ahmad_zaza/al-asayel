@extends('website.layouts.app')

@section('title')
    {{ $settings->title }}
@endsection

@section('content')
    <section class="gallery-title">
        <div class="container">
            <h1 class="main-title-underline">{{ __('gallery') }}</h1>
        </div>
    </section>
    <section class="gallery">
        <div class="container">
            <div id="mainGallery" class="row ourGallery">
                @foreach ($gallery_images as $key => $img)
                    <div id="gallery-{{ $key }}" class="col-lg-4 col-md-6 col-sm-12" style="padding-top: 25px;">
                        <a data-src="{{ asset($img->path) }}" class="gallery_item" target="_blank" data-lg-size="1600-2400"
                            style="cursor: pointer;">
                            <img id="gallery-img-{{ $key }}" class="gallery-image" src="{{ asset($img->path) }}"
                                loading="lazy">
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
