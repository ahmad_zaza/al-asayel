@extends('website.layouts.app')

@section('title')
    {{ $settings->title }}
@endsection

@section('content')
    @php
        $lang = App::getlocale();
    @endphp
    <section class="product-title">
        <div class="container">
            <h1 class="main-title-underline">{{ __('products') }}</h1>
        </div>
    </section>
    <section class="product-section">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <ul id="productGallery">
                        @foreach ($product_images as $image)
                            <li data-thumb="{{ asset($image->path) }}" data-src="{{ asset($image->path) }}">
                                <a data-src="{{ asset($image->path) }}" class="gallery_item" target="_blank"
                                    data-lg-size="1600-2400" style="cursor: pointer;">
                                    <img src="{{ asset($image->path) }}" alt="" loading="lazy">
                                </a>
                            </li>
                        @endforeach
                    </ul>
                    <div class="color">
                        <h6 style="margin-bottom:15px;">
                            {{ __('available_colors') }}
                        </h6>
                        <div class="product-colors">
                            @foreach ($product->productColors as $color)
                                <div class="center" style="background: {{ $color->code }};"></div>&nbsp;&nbsp;&nbsp;
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="description">
                        <h6>
                            {{ $product[$lang . '_name'] }}
                        </h6>
                        <p>
                            {{ $product[$lang . '_description'] }}
                        </p>
                    </div>
                    <div class="specification">
                        <h6 style="margin-bottom:15px;">
                            {{ __('technical_specification') }}
                        </h6>
                        <table class="spec_table table">
                            <thead class="spec-table-header">
                                <tr class="spec-table-row">
                                    <td>{{ __('name') }}</td>
                                    <td>{{ __('outer_diameter') }}</td>
                                    <td>{{ __('depth') }}</td>
                                    <td>{{ __('volume') }}</td>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($product->productDetails as $key => $prod_details)
                                    <tr
                                        class="spec-table-row {{ $key % 2 == 0 ? 'even-spec-table-row' : 'odd-spec-table-row' }}">
                                        <td>{{ $prod_details[$lang . '_name'] }}</td>
                                        <td>{{ $prod_details['outer_diameter'] }}</td>
                                        <td>{{ $prod_details['depth'] }}</td>
                                        <td>{{ $prod_details['volume'] }}</td>
                                    </tr>
                            </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
