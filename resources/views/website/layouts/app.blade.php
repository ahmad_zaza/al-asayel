<!DOCTYPE html>
@php
    $lang = App::getlocale();
@endphp
<html lang="{{ $lang }}">

<head>
    <meta charset="utf-8">
    <title>@yield('title')</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{ asset(CRUDBooster::getSetting('favicon')) }}" type="image/png" sizes="16x16">
    <link rel="shortcut icon" type="image/x-icon"
        href="{{ asset('images/WhatsApp Image 2023-04-29 at 17.16.24221.jpg') }}">
    <link href="{{ asset('css/style.css') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="{{ $settings->description }}">
    <meta name="keywords" content="{{ $settings->keywords }}">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nanum+Gothic:wght@400;700;800&display=swap" rel="stylesheet" />
    <!-- light slider-->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/lightGallery/css/lightslider.css') }}" />
    <link type="text/css" rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/2.6.0-beta.1/css/lightgallery.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/2.5.0/css/lg-pager.min.css"
        integrity="sha512-F4gJy0MBz7nwvFMIYNeap+CXaVQY0Y9pwsn/Up4mvOfjYD50u+8D7jIVZBkeHV7hZ2xjhY8uJRMkRzWQBvznRw=="
        crossorigin="anonymous" referrerpolicy="no-referrer">
    <link type="text/css" rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/2.6.0-beta.1/css/lg-zoom.min.css">
    <link type="text/css" rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/2.6.0-beta.1/css/lg-thumbnail.min.css">
    <!-- Vendor CSS Files -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css"
        integrity="sha512-5A8nwdMOWrSz20fDsjczgUidUBR8liPYU+WymTZP1lmY9G6Oc7HlZv156XqnsgNUzTyMefFTcsFH/tnJE/+xBg=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    {{-- Font Awesome  --}}
    <link rel="text/css" rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.5.0/css/brands.min.css" />
    {{-- Owl Carousel  --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"
        integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css"
        integrity="sha512-OTcub78R3msOCtY3Tc6FzeDJ8N9qvQn1Ph49ou13xgA9VsH9+LRxoFU6EqLhW4+PKRfU+/HReXmSZXHEkpYoOA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    {{-- Include RTL --}}
    @if ($lang == 'ar')
        <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.5.3/css/bootstrap.min.css"
            integrity="sha384-JvExCACAZcHNJEc7156QaHXTnQL3hQBixvj5RV5buE7vgnNEzzskDtx9NQ4p6BJe" crossorigin="anonymous">

        <link href="{{ asset('css/rtl_style.css') }}" rel="stylesheet" type="text/css" media="all" />
    @else
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
            integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    @endif
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js"
        integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.js"
        integrity="sha512-sww7U197vVXpRSffZdqfpqDU2SNoFvINLX4mXt1D6ZecxkhwcHmLj3QcL2cJ/aCxrTkUcaAa6EGmPK3Nfitygw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>

<body lang="{{ $lang }}">
    <div class="page-loader" id="loader">
        <div class="spinner">
            <div class="spinner-circle"></div>
            <div class="spinner-logo">
                <img src="https://alasayeldekorasyon.com/uploads/2023-05/asayel-logo-loader.webp" alt="Voila Logo">
            </div>
        </div>
    </div>
    <div class="wrapper">
        <!-- Navbar Section -->
        <header id="header" class="fixed-top d-flex align-items-center">
            <div class="container navbar-container">
                <nav id="navbar" class="navbar navbar-expand-lg">
                    <div class="row navbar-row" style="margin: 10px 0px;">
                        <div class="col-4" style="padding: 0px;">
                            <a class="navbar-brand navbar-right" href="{{ route('home.index') }}">
                                <img class="companyLogo" alt="CompanyLogo"
                                    src="{{ asset(CRUDBooster::getSetting('logo')) }}" loading="lazy">
                            </a>
                        </div>
                        <div class="col-8" style="padding: 0px; margin:auto; text-align: end">

                            <button class="navbar-toggler ml-auto float-xs-right" type="button"
                                data-toggle="collapse" data-target="#navbarAsayel" aria-controls="navbarAsayel"
                                aria-expanded="false" aria-label="Toggle navigation">
                                <i class="fa fa-bars" style="color:#f8c932; font-size:28px;" aria-hidden="true"></i>
                            </button>
                            <div class="collapse navbar-collapse" id="navbarAsayel">

                                <ul class="navbar-nav">
                                    @foreach ($menu as $key => $main_menu)
                                        @php
                                            $href = config('app.url') . '/' . $main_menu['section'];
                                            if (Request::fullUrl() == $href) {
                                                $str = $main_menu["name_$lang"];
                                            }
                                        @endphp
                                        <li class="nav-item main-nav-item ">
                                            <a id="nav-item nav-item-{{ $key }}"
                                                class="nav-link {{ Request::fullUrl() == $href || Request::fullUrl() . '/' == $href ? 'active-nav-link' : '' }}"
                                                href="{{ $href }}">
                                                {{ $main_menu["name_$lang"] }}
                                            </a>
                                        </li>
                                    @endforeach
                                    <li id="drop-down-nav" class="nav-item dropdown nav-dropdown-menu main-nav-item"
                                        style="margin-top:auto;">
                                        <div class="dropdown">
                                            <a href="#" class="nav-link dropdown-toggle lang-dropdown-toggle"
                                                type="button" id="dropdownMenuButton" data-toggle="dropdown"
                                                aria-haspopup="true" aria-expanded="false"
                                                aria-label="dropdown-aria">
                                                <i class="fa fa-globe" aria-hidden="true"></i> {{ App::getlocale() == 'ar' ? 'عر' : App::getlocale() }}
                                            </a>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton"
                                                style="margin-top:-5px;">
                                                @foreach ($languages as $lang)
                                                    <li>
                                                        <a class="dropdown-item"
                                                            href="{{ route('switchLang', [$lang->code]) }}">
                                                            {{ $lang->name }}
                                                        </a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </header>

        <div class="main">
            @yield('content')
            <a class="contact-span" href="https://api.whatsapp.com/send?phone=905413479225" target="blank">
                <div class="whatsapp">
                    <svg width="50px" height="50px" viewBox="0 0 24 20" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <g id="SVGRepo_bgCarrier" stroke-width="0"></g>
                        <g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g>
                        <g id="SVGRepo_iconCarrier">
                            <path
                                d="M17.6 6.31999C16.8669 5.58141 15.9943 4.99596 15.033 4.59767C14.0716 4.19938 13.0406 3.99622 12 3.99999C10.6089 4.00135 9.24248 4.36819 8.03771 5.06377C6.83294 5.75935 5.83208 6.75926 5.13534 7.96335C4.4386 9.16745 4.07046 10.5335 4.06776 11.9246C4.06507 13.3158 4.42793 14.6832 5.12 15.89L4 20L8.2 18.9C9.35975 19.5452 10.6629 19.8891 11.99 19.9C14.0997 19.9001 16.124 19.0668 17.6222 17.5816C19.1205 16.0965 19.9715 14.0796 19.99 11.97C19.983 10.9173 19.7682 9.87634 19.3581 8.9068C18.948 7.93725 18.3505 7.05819 17.6 6.31999ZM12 18.53C10.8177 18.5308 9.65701 18.213 8.64 17.61L8.4 17.46L5.91 18.12L6.57 15.69L6.41 15.44C5.55925 14.0667 5.24174 12.429 5.51762 10.8372C5.7935 9.24545 6.64361 7.81015 7.9069 6.80322C9.1702 5.79628 10.7589 5.28765 12.3721 5.37368C13.9853 5.4597 15.511 6.13441 16.66 7.26999C17.916 8.49818 18.635 10.1735 18.66 11.93C18.6442 13.6859 17.9355 15.3645 16.6882 16.6006C15.441 17.8366 13.756 18.5301 12 18.53ZM15.61 13.59C15.41 13.49 14.44 13.01 14.26 12.95C14.08 12.89 13.94 12.85 13.81 13.05C13.6144 13.3181 13.404 13.5751 13.18 13.82C13.07 13.96 12.95 13.97 12.75 13.82C11.6097 13.3694 10.6597 12.5394 10.06 11.47C9.85 11.12 10.26 11.14 10.64 10.39C10.6681 10.3359 10.6827 10.2759 10.6827 10.215C10.6827 10.1541 10.6681 10.0941 10.64 10.04C10.64 9.93999 10.19 8.95999 10.03 8.56999C9.87 8.17999 9.71 8.23999 9.58 8.22999H9.19C9.08895 8.23154 8.9894 8.25465 8.898 8.29776C8.8066 8.34087 8.72546 8.403 8.66 8.47999C8.43562 8.69817 8.26061 8.96191 8.14676 9.25343C8.03291 9.54495 7.98287 9.85749 8 10.17C8.0627 10.9181 8.34443 11.6311 8.81 12.22C9.6622 13.4958 10.8301 14.5293 12.2 15.22C12.9185 15.6394 13.7535 15.8148 14.58 15.72C14.8552 15.6654 15.1159 15.5535 15.345 15.3915C15.5742 15.2296 15.7667 15.0212 15.91 14.78C16.0428 14.4856 16.0846 14.1583 16.03 13.84C15.94 13.74 15.81 13.69 15.61 13.59Z"
                                fill="#ffffff"></path>
                        </g>
                    </svg>
                </div>
            </a>
        </div> <!-- Main -->
    </div><!-- Wrapper -->
    <section class="footer-section inner-section">
        <div class="top-footer-line footer-border"></div>
        <footer id="footer">
            <div class="container text-center footer-container">
                <p style="margin-bottom:0px;">
                    {{ __('all_rights') }}
                </p>
            </div>
        </footer>
        <div class="bottom-footer-line"></div>
    </section>

    <!-- Jquery and Js Plugins -->

    <script src="{{ asset('js/lightGallery/js/lightslider.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/2.6.0-beta.1/lightgallery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/2.5.0/plugins/pager/lg-pager.min.js"
        integrity="sha512-smL/gQDLzDjVmL1/tndmYmRLcSK1NibZsCkBeGk+bFwuJiIqv3Wck10bkTwlU8Bj99yPiMYroEvzSfD2Uxuy0g=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/2.6.0-beta.1/plugins/thumbnail/lg-thumbnail.min.js">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/2.6.0-beta.1/plugins/zoom/lg-zoom.min.js"></script>
    <script src="{{ asset('js/owlCarousel/owl.carousel.min.js') }}"></script>

    <script>
        document.addEventListener("DOMContentLoaded", function() {
            window.addEventListener('load', function() {

                document.querySelector(
                    "#loader").style.display = "none";
            });
            if ($(window).scrollTop() > 0) {
                $('#header').css('background', '#361c16');
            } else {
                $('#header').css('background',
                    'linear-gradient(rgba(71, 34, 25, 1) 0%, rgba(71, 34, 25, 0.3) 100%)');
            }
            $(window).scroll(function() {
                if ($(this).scrollTop() > 0) {
                    $('#header').css('background', '#361c16');
                } else {
                    $('#header').css('background',
                        'linear-gradient(rgba(71, 34, 25, 1) 0%, rgba(71, 34, 25, 0.3) 100%)');
                }
            });
        });
        $(document).ready(function() {
            // Navbar
            $('.navbar-nav .dropdown').hover(function() {
                $(this).find('.dropdown-menu').first().stop(true, true).slideDown(150);
            }, function() {
                $(this).find('.dropdown-menu').first().stop(true, true).slideUp(105)
            });
            // Clients
            $('.owl-carousel').owlCarousel({
                stagePadding: 0,
                nav: true,
                loop: true,
                margin: 10,
                responsiveClass: true,
                slideBy: 3,
                autoplay: true,
                autoplayTimeout: 5000,
                autoplayHoverPause: true,
                dotsData: false,
                lazyLoad: true,
                animateOut: true,
                rtl: true,
                responsive: {
                    0: {
                        items: 1,
                        nav: false
                    },
                    826: {
                        items: 2,
                        nav: false
                    },
                    1000: {
                        items: 3,
                        nav: true,
                        loop: true
                    },
                    1200: {
                        items: 4,
                        nav: true,
                        loop: true
                    },
                }
            });

            // Image Gallery
            $('#productGallery').lightSlider({
                gallery: true,
                item: 1,
                loop: true,
                thumbItem: 5,
                slideMargin: 0,
                enableDrag: false,
                currentPagerPosition: 'middle',
                controls: true,
                enableTouch: true,
            });

            lightGallery(document.getElementById('mainGallery'), {
                plugins: [lgZoom, lgThumbnail, lgPager],
                selector: '.gallery_item',
                // licenseKey: 'your_license_key',
                speed: 500,
                counter: true,
                numberOfSlideItemsInDom: 5,
                // ... other settings
            });

            // LightGallery
            lightGallery(document.getElementById('productGallery'), {
                plugins: [lgZoom, lgThumbnail, lgPager],
                selector: '.gallery_item',
                // licenseKey: 'your_license_key',
                speed: 500,
                counter: true,
                numberOfSlideItemsInDom: 5,
                // ... other settings
            });
        });
    </script>

</body>

</html>
