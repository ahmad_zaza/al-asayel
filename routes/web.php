<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/clear', function () {
// });
Route::middleware(['language'])->group(function () {
    Route::get('/', [HomeController::class, 'index'])->name('home.index');
    Route::get('/about_us', [HomeController::class, 'showAboutUs'])->name('about_us.index');
    Route::get('/gallery', [HomeController::class, 'showGallery'])->name('gallery.index');
    Route::get('/products', [ProductController::class, 'index'])->name('products.index');
    Route::get('/product/{id}', [ProductController::class, 'showProduct'])->name('product.index');
});
Route::post('/send-contact', [HomeController::class, 'sendContact'])->name('contact.send');
Route::get('/switch-lang/{code}', [HomeController::class, 'switchLanguage'])->name('switchLang');
